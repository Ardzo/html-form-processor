# Library for work with elements of HTML-forms

## Introduction
Several useful functions for work with elements of HTML-forms.

## Requirements
* PHP 4.1.0
* Windows or Unix

## Using
Copy `"HtmlFormProcessor.php"` from `"src"` onto server and use it in your script.  
Or use Composer - class will loads automatically.  
See example code in folder `"example"`.
