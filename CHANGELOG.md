# Version history

## **1.4** 2018-10-12
* Function `"getForm()"` is added
* Function `"getInput()"` can process all types of `"input"`
* Designed as a class
* All functions renamed

## **1.3.1** 2018-02-25
* Function `"fe_get_select()"` is fixed in case of
	using a set with values "" and "0".

## **1.3** 2010-01-07
* Extra parameters added in function `"fe_get_inputs_cr()"`

## **1.2** 2009-01-23
* Extra parameters added in function `"fe_get_select()"`
* `"foreach()"` is protected in functions

## **1.1** 2008-11-02
* Divider is added in function `"fe_get_inputs_cr()"`
* Extra parameters added in function `"fe_get_textarea()"`

## **1.0** 2008-05-21
* Initial release
