<?php
namespace Ardzo\HtmlForm;

require_once 'HtmlFormProcessor.php';

// Example 1: separate fields

$form_1 = new HtmlFormProcessor;
$form_1->width = '300px';

echo '
<form action="" method="POST">
'.$form_1->getInput('text', 'field_1', 'sample text', $form_1->width).'<br />
'.$form_1->getTextarea('field_2', 'sample text 2', $form_1->width).'<br />
'.$form_1->getInputsCr(0, 'field_3', array(1=>'Val 1',3=>'Val 2',4=>'Val 3'), 3).'<br />
'.$form_1->getSelect('field_4', array(1=>'Val 1',3=>'Val 2',4=>'Val 3'), 4).'<br />
'.$form_1->getInput('submit', 'save', 'Send a form').'<br />
</form>
';

// Example 2: whole form

$form_2 = new HtmlFormProcessor;
$form_2->width = '400px';
$form_2->action = '/';
$form_2->method = 'POST';
$form_2->extra = ' class="some_class"';

$hint_1 = 'Some hint 1';
$hint_2 = 'Some hint 2';

$form_template = '
<div class="line_1">
	<div class="title">[TITLE]:</div>
	<div class="data">
		[FIELD]
		<div class="hint">[HINT]</div>
	</div>
</div>
';

class TmpClass {};
$record = new TmpClass;
$record->login	= 'john';
$record->psw	= '123456';
$record->email	= 'john@example.com';
$record->msg	= 'Message';
$record->video	= 'YouTube code';

echo $form_2->getForm(
    array(
      'login'=>'Login~~~text~~~'.$hint_1,
      'psw'=>'Password~~~password',
      'email'=>'E-mail~~~email',
      'msg'=>'Record message~~~textarea~350px~100px',
      'video'=>'Code of video~~~textarea~~70px~~~'.$hint_2,
      'save'=>'Send~~~submit'
    ),
    $form_template, 'fields', $record
);

