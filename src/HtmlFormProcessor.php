<?php
/**
* Library for work with elements of HTML-forms
* 
* @version 1.4
* @copyright Copyright (C) 2008 - 2018 Richter
* @author Richter (richter@wpdom.com)
* @link http://wpdom.com
*/

namespace Ardzo\HtmlForm;

class HtmlFormProcessor
{
    public $width; // Width of elements of form
    public $action; // "action" of form
    public $method = 'GET'; // Method of form
    public $extra; // Additional parameters of form

    /**
    * Return a text-like "input"
    * @param string $type parameter "type" of element
    * @param string $name parameter "name" of element
    * @param string $value parameter "value" of element
    * @param string $size width for "style" with px or %
    * @param string $id parameter "id" of element
    * @param boolean $disabled TRUE or FALSE for parameter "disabled"
    * @param string $extra some additional suffix
    */
    public function getInput($type, $name, $value = '', $size = '', $id = '', $disabled = FALSE, $extra = '')
    {
        $out = '<input'.($id?' id="'.$id.'"':'').' type="'.$type.'" name="'.$name.'" value="'.$value.'"'.($size?' style="width: '.$size.'"':'').($disabled?' disabled="disabled"':'').' '.$extra.' />';
        return $out;
    }

    /**
    * Return a "textarea"
    * @param string $name parameter "name" of element
    * @param string $value parameter "value" of element
    * @param string $size_h width for "style" with px or %
    * @param string $size_v height for "style" with px or %
    * @param string $id parameter "id" of "input"
    * @param string $extra some additional suffix
    */
    public function getTextarea($name, $value = '', $size_h = '', $size_v = '', $id = '', $extra = '')
    {
        $out = '<textarea'.($id?' id="'.$id.'"':'').' name="'.$name.'" cols="" rows=""'.(($size_h or $size_v)?' style="width: '.$size_h.';height: '.$size_v.'"':'').' '.$extra.'>'.$value.'</textarea>';
        return $out;
    }

    /**
    * Return an "inputs" of types "checkboxes" or "radio"
    * @param boolean $type 0 - checkbox, 1 - radio
    * @param string $name parameter "name" of element
    * @param array $values set of values and texts
    * @param string $act_value selected value
    * @param string $default_value value if selected value is not set
    * @param string $divider some HTML between inputs
    * @param string $extra some additional suffix for each element
    */
    public function getInputsCr($type = '0', $name, $values, $act_value = '', $default_value = '', $divider = '', $extra = '')
    {
        $out = '';

        if ($act_value == '') $act_value = $default_value;
        $I = 0;
        if (is_array($values)) foreach ($values as $value=>$text) {
            $out .= '<input type="'.($type==0?'checkbox':'radio').'" name="'.$name.'" value="'.$value.'"';
            if (is_array($act_value)) {
                if (in_array($value, $act_value)) $out .= ' checked="checked"';
            } else {
                if ($act_value!='' && $value==$act_value) $out .= ' checked="checked"';
            }
            $out .=  ' '.$extra.' /> '.$text;
            if ($I<count($values)-1) $out .= $divider;
            $I++;
        }
        return $out;
    }

    /**
    * Return a "select"
    * @param string $name parameter "name" of element
    * @param array $values set of values and texts
    * @param string $act_value selected value
    * @param int $size if >1 then will add a "multiple" parameter
    * @param string $default_value value if selected value is not set
    * @param string $id parameter "id" of element
    * @param string $extra some additional suffix for each element
    */
    public function getSelect($name, $values, $act_value, $size = 1, $default_value = '', $id = '', $extra = '')
    {
        if (!is_numeric($size)) $size = 1;
        if (empty($act_value) && $act_value!='0') $act_value = $default_value;

        $out = '<select'.($id?' id="'.$id.'"':'').' name="'.$name.'"'.($size>1?' multiple="multiple" size="'.$size.'"':'').' '.$extra.'>';
        if (is_array($values)) foreach ($values as $value=>$text) {
            $out .= '<option value="'.$value.'"';
            if (is_array($act_value)) {
                if (in_array(strval($value), $act_value)) $out .= ' selected="selected"';
            } else {
                if ($act_value!='' && $value==$act_value) $out .= ' selected="selected"';
            }
            $out .= '>'.$text.'</option>';
        }
        $out .= '</select>';
        return $out;
    }

    /**
    * Return a whole form
    * @param array $fields set of fields
    * @param string $template HTML-template of one element of form
    * @param array $fields_var name of array for output in "name" of elements
    * @param string $source_obj values of fields (object)
    * @return string HTML-form
    */
    public function getForm($fields, $template, $fields_var, $source_obj)
    {
        $out = '<form action="'.$this->action.'" method="'.$this->method.'"'.($this->extra?$this->extra:'').'>';

        foreach ($fields as $I=>$val) {
            $tmp_1 = explode('~~~', $val);
            $tmp_2 = explode('~', $tmp_1[1]);

            $tmp_field = '';
            switch ($tmp_2[0]) {
                case 'text': 
                case 'password': 
                case 'email': 
                    $tmp_field = $this->getInput($tmp_2[0], $fields_var.'['.$I.']', htmlspecialchars($source_obj->$I), $this->width?$this->width:'', '', '', @$tmp_1[3]);
                    break;
                case 'submit': 
                    $tmp_field = $this->getInput($tmp_2[0], $I, $tmp_1[0], '', '', '', @$tmp_1[3]);
                    break;
                case 'textarea': 
                    $tmp_field = $this->getTextarea($fields_var.'['.$I.']', $source_obj->$I, ($tmp_2[1]?$tmp_2[1]:$this->width), $tmp_2[2]);
                    break;
                case 'select':
                    $tmp_field = $this->getSelect($fields_var.'['.$I.']'.($tmp_2[2]>1?'[]':''), $GLOBALS[$tmp_2[1]], $source_obj->$I, $tmp_2[2]);
                    break;
                case 'checkbox':
                    $tmp_field = '<input type="checkbox" name="'.$fields_var.'['.$I.']" value="1"'.($source_obj->$I?' checked="checked"':'').' />';
                    break;
            }

            $tmp_tmpl = $template;
            $out .= str_replace(array('[TITLE]', '[FIELD]', '[HINT]'), array($tmp_1[0], $tmp_field, @$tmp_1[2]), $tmp_tmpl);
        }
        $out .= '</form>';
        return $out;
    }
}
?>
